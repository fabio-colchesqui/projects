package br.com.moip.request;

/**
 * The Class TransactionRequest.
 */
public class TransactionRequest {

	/** The payment request. */
	private PaymentRequest paymentRequest;

	/**
	 * Payment.
	 *
	 * @param paymentRequest
	 *            the payment request
	 * @return the transaction request
	 */
	public TransactionRequest payment(PaymentRequest paymentRequest) {
		this.paymentRequest = paymentRequest;
		return this;
	}

	/**
	 * Gets the payment request.
	 *
	 * @return the payment request
	 */
	public PaymentRequest getPaymentRequest() {
		return paymentRequest;
	}

	/**
	 * Sets the payment request.
	 *
	 * @param paymentRequest
	 *            the new payment request
	 */
	public void setPaymentRequest(PaymentRequest paymentRequest) {
		this.paymentRequest = paymentRequest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
