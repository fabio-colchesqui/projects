package br.com.moip.request;

/**
 * The Class PaymentMethodRequest.
 */
public class PaymentMethodRequest {

	/** The method type. */
	private MethodType methodType;

	/** The card request. */
	private CardRequest cardRequest;

	/**
	 * Credit card.
	 *
	 * @param cardRequest
	 *            the card request
	 * @return the payment method request
	 */
	public PaymentMethodRequest creditCard(CardRequest cardRequest) {
		this.cardRequest = cardRequest;
		this.methodType = MethodType.CREDIT_CARD;
		return this;
	}

	/**
	 * Debit card.
	 *
	 * @param cardRequest
	 *            the card request
	 * @return the payment method request
	 */
	public PaymentMethodRequest debitCard(CardRequest cardRequest) {
		this.cardRequest = cardRequest;
		this.methodType = MethodType.DEBIT_CARD;
		return this;
	}

	/**
	 * Boleto.
	 *
	 * @return the payment method request
	 */
	public PaymentMethodRequest boleto() {
		this.methodType = MethodType.DEBIT_TICKET;
		return this;
	}

	/**
	 * Gets the method type.
	 *
	 * @return the method type
	 */
	public MethodType getMethodType() {
		return methodType;
	}

	/**
	 * Sets the method type.
	 *
	 * @param methodType
	 *            the new method type
	 */
	public void setMethodType(MethodType methodType) {
		this.methodType = methodType;
	}

	/**
	 * Gets the card request.
	 *
	 * @return the card request
	 */
	public CardRequest getCardRequest() {
		return cardRequest;
	}

	/**
	 * Sets the card request.
	 *
	 * @param cardRequest
	 *            the new card request
	 */
	public void setCardRequest(CardRequest cardRequest) {
		this.cardRequest = cardRequest;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
