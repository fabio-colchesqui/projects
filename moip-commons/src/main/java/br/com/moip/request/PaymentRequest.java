package br.com.moip.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

/**
 * The Class PaymentRequest.
 */
public class PaymentRequest {

	/** The client request. */
	@NotNull
	private ClientRequest clientRequest;

	/** The buyer request. */
	@NotNull
	private BuyerRequest buyerRequest;

	/** The payment method request. */
	private PaymentMethodRequest paymentMethodRequest;

	/** The total amount. */
	@NotNull
	private BigDecimal totalAmount;

	/**
	 * Client.
	 *
	 * @param clientRequest
	 *            the client request
	 * @return the payment request
	 */
	public PaymentRequest client(ClientRequest clientRequest) {
		this.clientRequest = clientRequest;
		return this;
	}

	/**
	 * Buyer.
	 *
	 * @param buyerRequest
	 *            the buyer request
	 * @return the payment request
	 */
	public PaymentRequest buyer(BuyerRequest buyerRequest) {
		this.buyerRequest = buyerRequest;
		return this;
	}

	/**
	 * Payment method.
	 *
	 * @param paymentMethodRequest
	 *            the payment method request
	 * @return the payment request
	 */
	public PaymentRequest paymentMethod(PaymentMethodRequest paymentMethodRequest) {
		this.paymentMethodRequest = paymentMethodRequest;
		return this;
	}

	/**
	 * Total amount.
	 *
	 * @param totalAmount
	 *            the total amount
	 * @return the payment request
	 */
	public PaymentRequest totalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
		return this;
	}

	/**
	 * Gets the client request.
	 *
	 * @return the client request
	 */
	public ClientRequest getClientRequest() {
		return clientRequest;
	}

	/**
	 * Gets the buyer request.
	 *
	 * @return the buyer request
	 */
	public BuyerRequest getBuyerRequest() {
		return buyerRequest;
	}

	/**
	 * Gets the payment method request.
	 *
	 * @return the payment method request
	 */
	public PaymentMethodRequest getPaymentMethodRequest() {
		return paymentMethodRequest;
	}

	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
