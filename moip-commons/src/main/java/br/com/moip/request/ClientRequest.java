package br.com.moip.request;

import javax.validation.constraints.NotNull;

/**
 * The Class ClientRequest.
 */
public class ClientRequest {

	/** The id client. */
	@NotNull
	private Long idClient;

	/**
	 * Client.
	 *
	 * @param idClient
	 *            the id client
	 * @return the client request
	 */
	public ClientRequest client(Long idClient) {
		this.idClient = idClient;
		return this;
	}

	/**
	 * Gets the id client.
	 *
	 * @return the id client
	 */
	public Long getIdClient() {
		return idClient;
	}

	/**
	 * Sets the id client.
	 *
	 * @param idClient
	 *            the new id client
	 */
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
