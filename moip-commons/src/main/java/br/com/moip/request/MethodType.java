package br.com.moip.request;

/**
 * The Enum MethodType.
 */
public enum MethodType {

	/** The credit card. */
	CREDIT_CARD,
	/** The debit card. */
	DEBIT_CARD,
	/** The debit ticket. */
	DEBIT_TICKET;

}
