package br.com.moip.request;

/**
 * The Class CardRequest.
 */
public class CardRequest {

	/** The number. */
	private Long number;
	
	/** The name. */
	private String name;
	
	/** The expiration date. */
	private String expirationDate;
	
	/** The cvv. */
	private Long cvv;

	/**
	 * Number.
	 *
	 * @param number the number
	 * @return the card request
	 */
	public CardRequest number(Long number) {
		this.number = number;
		return this;
	}

	/**
	 * Name.
	 *
	 * @param name the name
	 * @return the card request
	 */
	public CardRequest name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Expiration date.
	 *
	 * @param expirationDate the expiration date
	 * @return the card request
	 */
	public CardRequest expirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
		return this;
	}

	/**
	 * Cvv.
	 *
	 * @param cvv the cvv
	 * @return the card request
	 */
	public CardRequest cvv(Long cvv) {
		this.cvv = cvv;
		return this;
	}

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public Long getNumber() {
		return number;
	}

	/**
	 * Sets the number.
	 *
	 * @param number the new number
	 */
	public void setNumber(Long number) {
		this.number = number;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate the new expiration date
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the cvv.
	 *
	 * @return the cvv
	 */
	public Long getCvv() {
		return cvv;
	}

	/**
	 * Sets the cvv.
	 *
	 * @param cvv the new cvv
	 */
	public void setCvv(Long cvv) {
		this.cvv = cvv;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
