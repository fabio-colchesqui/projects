package br.com.moip.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * The Class BuyerRequest.
 */
public class BuyerRequest {

	/** The name. */
	@NotNull
	private String name;

	/** The email. */
	@NotNull
	@Pattern(regexp = ".+@.+\\.[a-z]+")
	private String email;

	/** The cpf. */
	@NotNull
	private String cpf;

	/**
	 * Buyer name.
	 *
	 * @param name
	 *            the name
	 * @return the buyer request
	 */
	public BuyerRequest buyerName(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Buyer email.
	 *
	 * @param email
	 *            the email
	 * @return the buyer request
	 */
	public BuyerRequest buyerEmail(String email) {
		this.email = email;
		return this;
	}

	/**
	 * Buyer cpf.
	 *
	 * @param cpf
	 *            the cpf
	 * @return the buyer request
	 */
	public BuyerRequest buyerCpf(String cpf) {
		this.cpf = cpf;
		return this;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the cpf.
	 *
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Sets the cpf.
	 *
	 * @param cpf
	 *            the new cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
