package br.com.moip.response;

/**
 * The Class PaymentResponse.
 */
public class PaymentResponse {

	/** The id client. */
	private Long idClient;

	/** The amount reponse. */
	private AmountResponse amountReponse;

	/** The boleto response. */
	private BoletoResponse boletoResponse;

	/** The authorization status response. */
	private AuthorizationStatusResponse authorizationStatusResponse;

	/** The buyer response. */
	private BuyerResponse buyerResponse;

	/**
	 * Gets the id client.
	 *
	 * @return the id client
	 */
	public Long getIdClient() {
		return idClient;
	}

	/**
	 * Sets the id client.
	 *
	 * @param idClient
	 *            the new id client
	 */
	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	/**
	 * Gets the amount reponse.
	 *
	 * @return the amount reponse
	 */
	public AmountResponse getAmountReponse() {
		return amountReponse;
	}

	/**
	 * Sets the amount reponse.
	 *
	 * @param amountReponse
	 *            the new amount reponse
	 */
	public void setAmountReponse(AmountResponse amountReponse) {
		this.amountReponse = amountReponse;
	}

	/**
	 * Gets the boleto response.
	 *
	 * @return the boleto response
	 */
	public BoletoResponse getBoletoResponse() {
		return boletoResponse;
	}

	/**
	 * Sets the boleto response.
	 *
	 * @param boletoResponse
	 *            the new boleto response
	 */
	public void setBoletoResponse(BoletoResponse boletoResponse) {
		this.boletoResponse = boletoResponse;
	}

	/**
	 * Gets the authorization status response.
	 *
	 * @return the authorization status response
	 */
	public AuthorizationStatusResponse getAuthorizationStatusResponse() {
		return authorizationStatusResponse;
	}

	/**
	 * Sets the authorization status response.
	 *
	 * @param authorizationStatusResponse
	 *            the new authorization status response
	 */
	public void setAuthorizationStatusResponse(AuthorizationStatusResponse authorizationStatusResponse) {
		this.authorizationStatusResponse = authorizationStatusResponse;
	}

	/**
	 * Gets the buyer response.
	 *
	 * @return the buyer response
	 */
	public BuyerResponse getBuyerResponse() {
		return buyerResponse;
	}

	/**
	 * Sets the buyer response.
	 *
	 * @param buyerResponse
	 *            the new buyer response
	 */
	public void setBuyerResponse(BuyerResponse buyerResponse) {
		this.buyerResponse = buyerResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
