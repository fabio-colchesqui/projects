package br.com.moip.response;

/**
 * The Enum AuthorizationStatusResponse.
 */
public enum AuthorizationStatusResponse {

	/** The approved. */
	APPROVED,
	/** The declined. */
	DECLINED,
	/** The pending. */
	PENDING;

}
