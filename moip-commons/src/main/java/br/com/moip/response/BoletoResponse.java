package br.com.moip.response;

/**
 * The Class BoletoResponse.
 */
public class BoletoResponse {

	/** The line code. */
	private String lineCode;

	/** The expiration date. */
	private String expirationDate;

	/**
	 * Gets the line code.
	 *
	 * @return the line code
	 */
	public String getLineCode() {
		return lineCode;
	}

	/**
	 * Sets the line code.
	 *
	 * @param lineCode
	 *            the new line code
	 */
	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate
	 *            the new expiration date
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
