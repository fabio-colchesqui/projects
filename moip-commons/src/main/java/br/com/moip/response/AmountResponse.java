package br.com.moip.response;

import java.math.BigDecimal;

/**
 * The Class AmountResponse.
 */
public class AmountResponse {

	/** The total amount. */
	private BigDecimal totalAmount;

	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * Sets the total amount.
	 *
	 * @param totalAmount
	 *            the new total amount
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
