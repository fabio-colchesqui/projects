package br.com.moip;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.moip.entity.Amount;
import br.com.moip.entity.AuthorizationStatus;
import br.com.moip.entity.Boleto;
import br.com.moip.entity.Buyer;
import br.com.moip.entity.Client;
import br.com.moip.entity.CreditCard;
import br.com.moip.entity.DebitCard;
import br.com.moip.entity.Payment;
import br.com.moip.entity.PaymentType;
import br.com.moip.entity.ProcessPayment;
import br.com.moip.exception.ProcessPaymentException;
import br.com.moip.utils.BoletoMock;

/**
 * The Class ProcessPaymentRepositoryTest.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProcessPaymentRepositoryTest {

	/** The entity manager. */
	@Autowired
	private TestEntityManager entityManager;

	/**
	 * Process payment boleto.
	 *
	 * @throws ProcessPaymentException
	 *             the process payment exception
	 */
	@Test
	public void processPaymentBoleto() throws ProcessPaymentException {

		ProcessPayment processPayment = new ProcessPayment();
		processPayment.setBuyer(new Buyer());
		processPayment.getBuyer().setCpf("200.201.202-20");
		processPayment.getBuyer().setEmail("jose.almeida@teste.com.br");
		processPayment.getBuyer().setName("JOSE ALMEIDA");

		processPayment.setClient(new Client());
		processPayment.getClient().setId(1001L);

		processPayment.setPayment(new Payment());
		processPayment.getPayment().setAmount(new Amount());
		processPayment.getPayment().getAmount().setTotalAmount(new BigDecimal("1000.00"));

		createDebitTicket(processPayment);

		ProcessPayment merge = entityManager.merge(processPayment);

		assertNotNull(merge);
	}

	/**
	 * Process payment credit card.
	 *
	 * @throws ProcessPaymentException
	 *             the process payment exception
	 */
	@Test
	public void processPaymentCreditCard() throws ProcessPaymentException {

		ProcessPayment processPayment = new ProcessPayment();
		processPayment.setBuyer(new Buyer());
		processPayment.getBuyer().setCpf("200.201.202-20");
		processPayment.getBuyer().setEmail("jose.almeida@teste.com.br");
		processPayment.getBuyer().setName("JOSE ALMEIDA");

		processPayment.setClient(new Client());
		processPayment.getClient().setId(1001L);

		processPayment.setPayment(new Payment());
		processPayment.getPayment().setAmount(new Amount());
		processPayment.getPayment().getAmount().setTotalAmount(new BigDecimal("1000.00"));

		createCreditCard(processPayment);

		ProcessPayment merge = entityManager.merge(processPayment);

		assertNotNull(merge);
	}

	/**
	 * Process payment debit card.
	 *
	 * @throws ProcessPaymentException
	 *             the process payment exception
	 */
	@Test
	public void processPaymentDebitCard() throws ProcessPaymentException {

		ProcessPayment processPayment = new ProcessPayment();
		processPayment.setBuyer(new Buyer());
		processPayment.getBuyer().setCpf("200.201.202-20");
		processPayment.getBuyer().setEmail("jose.almeida@teste.com.br");
		processPayment.getBuyer().setName("JOSE ALMEIDA");

		processPayment.setClient(new Client());
		processPayment.getClient().setId(1001L);

		processPayment.setPayment(new Payment());
		processPayment.getPayment().setAmount(new Amount());
		processPayment.getPayment().getAmount().setTotalAmount(new BigDecimal("1000.00"));

		createDebitCard(processPayment);

		entityManager.merge(processPayment);

		ProcessPayment merge = entityManager.merge(processPayment);

		assertNotNull(merge);
	}

	/**
	 * Creates the debit ticket.
	 *
	 * @param processPayment
	 *            the process payment
	 * @throws ProcessPaymentException
	 *             the process payment exception
	 */
	private void createDebitTicket(ProcessPayment processPayment) throws ProcessPaymentException {
		Boleto boleto = new Boleto();
		boleto.setExpirationDate(BoletoMock.generateExpirationDate());
		boleto.setNumberBoleto(BoletoMock.generateBoletoNumber());
		processPayment.getPayment().setPaymentType(PaymentType.DEBIT_TICKET);
		processPayment.getPayment().setBoleto(boleto);
		processPayment.setAuthorizationStatus(AuthorizationStatus.PENDING);
	}

	/**
	 * Creates the debit card.
	 *
	 * @param processPayment
	 *            the process payment
	 * @throws ProcessPaymentException
	 *             the process payment exception
	 */
	private void createDebitCard(ProcessPayment processPayment) throws ProcessPaymentException {
		DebitCard debitCard = new DebitCard();
		debitCard.setExpirationDate(new Date());
		debitCard.setName("JOSE ALMEIDA");
		debitCard.setNumber(100234679009209L);
		processPayment.getPayment().setPaymentType(PaymentType.DEBIT_CARD);
		processPayment.getPayment().setCard(debitCard);
		processPayment.setAuthorizationStatus(AuthorizationStatus.APPROVED);
	}

	/**
	 * Creates the credit card.
	 *
	 * @param processPayment
	 *            the process payment
	 * @throws ProcessPaymentException
	 *             the process payment exception
	 */
	private void createCreditCard(ProcessPayment processPayment) throws ProcessPaymentException {
		CreditCard creditCard = new CreditCard();
		creditCard.setCvv(111L);
		creditCard.setExpirationDate(new Date());
		creditCard.setName("JOSE ALMEIDA");
		creditCard.setNumber(100234679009209L);
		processPayment.getPayment().setPaymentType(PaymentType.CREDIT_CARD);
		processPayment.getPayment().setCard(creditCard);
		processPayment.setAuthorizationStatus(AuthorizationStatus.APPROVED);
	}

}
