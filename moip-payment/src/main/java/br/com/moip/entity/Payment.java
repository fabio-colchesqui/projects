package br.com.moip.entity;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The Class Payment.
 */
@Entity
@Table(name = "Payment")
@DiscriminatorColumn(name = "paymentmode")
public class Payment {

	/** The payment id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long paymentId;

	/** The card. */
	@OneToOne(optional = true, cascade = CascadeType.ALL)
	private Card card;

	/** The boleto. */
	@OneToOne(optional = true, cascade = CascadeType.ALL)
	private Boleto boleto;

	/** The amount. */
	@OneToOne(optional = true, cascade = CascadeType.ALL)
	private Amount amount;

	/** The payment type. */
	@Enumerated(EnumType.ORDINAL)
	private PaymentType paymentType;

	/**
	 * Gets the payment id.
	 *
	 * @return the payment id
	 */
	public Long getPaymentId() {
		return paymentId;
	}

	/**
	 * Sets the payment id.
	 *
	 * @param paymentId
	 *            the new payment id
	 */
	public void setPaymentId(Long paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * Gets the card.
	 *
	 * @return the card
	 */
	public Card getCard() {
		return card;
	}

	/**
	 * Sets the card.
	 *
	 * @param card
	 *            the new card
	 */
	public void setCard(Card card) {
		this.card = card;
	}

	/**
	 * Gets the boleto.
	 *
	 * @return the boleto
	 */
	public Boleto getBoleto() {
		return boleto;
	}

	/**
	 * Sets the boleto.
	 *
	 * @param boleto
	 *            the new boleto
	 */
	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public Amount getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(Amount amount) {
		this.amount = amount;
	}

	/**
	 * Gets the payment type.
	 *
	 * @return the payment type
	 */
	public PaymentType getPaymentType() {
		return paymentType;
	}

	/**
	 * Sets the payment type.
	 *
	 * @param paymentType
	 *            the new payment type
	 */
	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((paymentId == null) ? 0 : paymentId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (paymentId == null) {
			if (other.paymentId != null)
				return false;
		} else if (!paymentId.equals(other.paymentId))
			return false;
		return true;
	}

}