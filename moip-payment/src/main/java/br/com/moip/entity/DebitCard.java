package br.com.moip.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * The Class DebitCard.
 */
@Entity
@DiscriminatorValue("debitcard")
@PrimaryKeyJoinColumn(name = "cardId")
public class DebitCard extends Card {

	/** The number. */
	@Column(name = "number")
	private Long number;

	/** The name. */
	@Column(name = "name")
	private String name;

	/** The expiration date. */
	@Column(name = "expirationDate")
	private Date expirationDate;

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public Long getNumber() {
		return number;
	}

	/**
	 * Sets the number.
	 *
	 * @param number
	 *            the new number
	 */
	public void setNumber(Long number) {
		this.number = number;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate
	 *            the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

}