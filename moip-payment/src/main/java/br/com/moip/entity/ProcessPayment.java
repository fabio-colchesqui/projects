package br.com.moip.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * The Class ProcessPayment.
 */
@Entity
public class ProcessPayment {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/** The buyer. */
	@OneToOne(optional = true, cascade = CascadeType.ALL)
	private Buyer buyer;

	/** The client. */
	@OneToOne(optional = true, cascade = CascadeType.ALL)
	private Client client;

	/** The payment. */
	@OneToOne(optional = true, cascade = CascadeType.ALL)
	private Payment payment;

	/** The authorization status. */
	@Enumerated(EnumType.ORDINAL)
	private AuthorizationStatus authorizationStatus;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the buyer.
	 *
	 * @return the buyer
	 */
	public Buyer getBuyer() {
		return buyer;
	}

	/**
	 * Sets the buyer.
	 *
	 * @param buyer the new buyer
	 */
	public void setBuyer(Buyer buyer) {
		this.buyer = buyer;
	}

	/**
	 * Gets the client.
	 *
	 * @return the client
	 */
	public Client getClient() {
		return client;
	}

	/**
	 * Sets the client.
	 *
	 * @param client the new client
	 */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
	 * Gets the payment.
	 *
	 * @return the payment
	 */
	public Payment getPayment() {
		return payment;
	}

	/**
	 * Sets the payment.
	 *
	 * @param payment the new payment
	 */
	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	/**
	 * Gets the authorization status.
	 *
	 * @return the authorization status
	 */
	public AuthorizationStatus getAuthorizationStatus() {
		return authorizationStatus;
	}

	/**
	 * Sets the authorization status.
	 *
	 * @param authorizationStatus the new authorization status
	 */
	public void setAuthorizationStatus(AuthorizationStatus authorizationStatus) {
		this.authorizationStatus = authorizationStatus;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProcessPayment other = (ProcessPayment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
