package br.com.moip.entity;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * The Class Card.
 */
@Entity
@Table(name = "Card")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "cardmode")
public class Card {

	/** The card id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int cardId;

	/**
	 * Gets the card id.
	 *
	 * @return the card id
	 */
	public int getCardId() {
		return cardId;
	}

	/**
	 * Sets the card id.
	 *
	 * @param cardId the new card id
	 */
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cardId;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (cardId != other.cardId)
			return false;
		return true;
	}

}
