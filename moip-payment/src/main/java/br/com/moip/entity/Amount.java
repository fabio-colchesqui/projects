package br.com.moip.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * The Class Amount.
 */
@Entity
public class Amount {

	/** The amount id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long amountId;

	/** The total amount. */
	private BigDecimal totalAmount;

	/**
	 * Gets the amount id.
	 *
	 * @return the amount id
	 */
	public Long getAmountId() {
		return amountId;
	}

	/**
	 * Sets the amount id.
	 *
	 * @param amountId
	 *            the new amount id
	 */
	public void setAmountId(Long amountId) {
		this.amountId = amountId;
	}

	/**
	 * Gets the total amount.
	 *
	 * @return the total amount
	 */
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	/**
	 * Sets the total amount.
	 *
	 * @param totalAmount
	 *            the new total amount
	 */
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amountId == null) ? 0 : amountId.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Amount other = (Amount) obj;
		if (amountId == null) {
			if (other.amountId != null)
				return false;
		} else if (!amountId.equals(other.amountId))
			return false;
		return true;
	}

}
