package br.com.moip.entity;

/**
 * The Enum PaymentType.
 */
public enum PaymentType {

	/** The credit card. */
	CREDIT_CARD(1),
	/** The debit card. */
	DEBIT_CARD(2),
	/** The debit ticket. */
	DEBIT_TICKET(3);

	/** The code. */
	private final int code;

	/**
	 * Instantiates a new payment type.
	 *
	 * @param code
	 *            the code
	 */
	private PaymentType(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

}
