package br.com.moip.entity;

import java.util.Random;

/**
 * The Enum AuthorizationStatus.
 */
public enum AuthorizationStatus {

	/** The approved. */
	APPROVED,
	/** The declined. */
	DECLINED,
	/** The pending. */
	PENDING;

	/** The Constant VALUES. */
	private static final AuthorizationStatus[] VALUES = values();

	/** The Constant SIZE. */
	private static final int SIZE = VALUES.length;

	/** The Constant RANDOM. */
	private static final Random RANDOM = new Random();

	/**
	 * Gets the random.
	 *
	 * @return the random
	 */
	public static AuthorizationStatus getRandom() {
		return VALUES[RANDOM.nextInt(SIZE)];
	}
}
