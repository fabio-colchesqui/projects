package br.com.moip.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * The Class Boleto.
 */
@Entity
public class Boleto {

	/** The boleto id. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int boletoId;

	/** The number boleto. */
	private String numberBoleto;

	/** The expiration date. */
	private Date expirationDate;

	/**
	 * Gets the boleto id.
	 *
	 * @return the boleto id
	 */
	public int getBoletoId() {
		return boletoId;
	}

	/**
	 * Sets the boleto id.
	 *
	 * @param boletoId
	 *            the new boleto id
	 */
	public void setBoletoId(int boletoId) {
		this.boletoId = boletoId;
	}

	/**
	 * Gets the number boleto.
	 *
	 * @return the number boleto
	 */
	public String getNumberBoleto() {
		return numberBoleto;
	}

	/**
	 * Sets the number boleto.
	 *
	 * @param numberBoleto
	 *            the new number boleto
	 */
	public void setNumberBoleto(String numberBoleto) {
		this.numberBoleto = numberBoleto;
	}

	/**
	 * Gets the expiration date.
	 *
	 * @return the expiration date
	 */
	public Date getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expiration date.
	 *
	 * @param expirationDate
	 *            the new expiration date
	 */
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + boletoId;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Boleto other = (Boleto) obj;
		if (boletoId != other.boletoId)
			return false;
		return true;
	}

}
