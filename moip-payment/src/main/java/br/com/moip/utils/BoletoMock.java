package br.com.moip.utils;

import java.util.Date;
import java.util.Random;

/**
 * The Class BoletoMock.
 */
public class BoletoMock {

	/**
	 * Generate boleto number.
	 *
	 * @return the string
	 */
	public static String generateBoletoNumber() {
		Random rand = new Random();
		int randomNum = rand.nextInt((5 - 1) + 1) + 1;
		String line = "";

		switch (randomNum) {
		case 1:
			line = "03399.89061 46730.001878 98215.301118 9 74370000054286";
			break;
		case 2:
			line = "03398.89513 46870.001978 98578.376718 8 78520000086778";
			break;
		case 3:
			line = "03393.89678 46549.001956 98389.302787 8 64560000035601";
			break;
		case 4:
			line = "03298.89123 46785.001907 98459.302849 9 87980000087623";
			break;
		case 5:
			line = "02780.89012 46756.001898 98288.303031 9 78340000078992";
			break;
		}

		return line;
	}

	/**
	 * Generate expiration date.
	 *
	 * @return the date
	 */
	public static Date generateExpirationDate() {
		Random rnd = new Random();
		return new Date(Math.abs(System.currentTimeMillis() - rnd.nextLong()));
	}

}
