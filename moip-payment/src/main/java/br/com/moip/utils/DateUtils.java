package br.com.moip.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class DateUtils.
 */
public class DateUtils {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(DateUtils.class);

	/**
	 * To date from string.
	 *
	 * @param date
	 *            the date
	 * @return the date
	 */
	public static Date toDateFromString(String date) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			return formatter.parse(date);
		} catch (ParseException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * To string from date.
	 *
	 * @param date
	 *            the date
	 * @return the string
	 */
	public static String toStringFromDate(Date date) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			return formatter.format(date);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}

}
