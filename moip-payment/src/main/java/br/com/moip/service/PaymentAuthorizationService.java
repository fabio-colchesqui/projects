package br.com.moip.service;

import org.springframework.stereotype.Component;

import br.com.moip.entity.AuthorizationStatus;
import br.com.moip.entity.Card;

/**
 * The Class PaymentAuthorizationService.
 */
@Component
public class PaymentAuthorizationService {

	/**
	 * Authorization process.
	 *
	 * @param card
	 *            the card
	 * @return the authorization status
	 */
	public AuthorizationStatus authorizationProcess(Card card) {
		return AuthorizationStatus.getRandom();
	}

}
