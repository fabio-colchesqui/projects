package br.com.moip.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.moip.entity.Amount;
import br.com.moip.entity.AuthorizationStatus;
import br.com.moip.entity.Boleto;
import br.com.moip.entity.Buyer;
import br.com.moip.entity.Client;
import br.com.moip.entity.CreditCard;
import br.com.moip.entity.DebitCard;
import br.com.moip.entity.Payment;
import br.com.moip.entity.PaymentType;
import br.com.moip.entity.ProcessPayment;
import br.com.moip.exception.ProcessPaymentException;
import br.com.moip.repository.BuyerRepository;
import br.com.moip.repository.ClientRepository;
import br.com.moip.repository.ProcessPaymentRepository;
import br.com.moip.request.PaymentRequest;
import br.com.moip.utils.BoletoMock;
import br.com.moip.utils.DateUtils;

/**
 * The Class ProcessPaymentService.
 */
@Component
public class ProcessPaymentService {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(ProcessPaymentService.class);

	/** The process payment repository. */
	@Autowired
	private ProcessPaymentRepository processPaymentRepository;

	/** The buyer repository. */
	@Autowired
	private BuyerRepository buyerRepository;

	/** The client repository. */
	@Autowired
	private ClientRepository clientRepository;

	/** The payment authorization service. */
	@Autowired
	private PaymentAuthorizationService paymentAuthorizationService;

	/**
	 * Process payment.
	 *
	 * @param paymentRequest
	 *            the payment request
	 * @return the process payment
	 */
	public ProcessPayment processPayment(PaymentRequest paymentRequest) {
		try {
			ProcessPayment processPayment = new ProcessPayment();
			Buyer buyer = buyerRepository.findByCpf(paymentRequest.getBuyerRequest().getCpf());
			if (buyer == null) {
				processPayment.setBuyer(new Buyer());
				processPayment.getBuyer().setCpf(paymentRequest.getBuyerRequest().getCpf());
				processPayment.getBuyer().setEmail(paymentRequest.getBuyerRequest().getEmail());
				processPayment.getBuyer().setName(paymentRequest.getBuyerRequest().getName());
			} else {
				processPayment.setBuyer(buyer);
			}

			Optional<Client> client = clientRepository.findById(paymentRequest.getClientRequest().getIdClient());
			processPayment.setClient(client.get());

			processPayment.setPayment(new Payment());
			processPayment.getPayment().setAmount(new Amount());
			processPayment.getPayment().getAmount().setTotalAmount(paymentRequest.getTotalAmount());

			switch (paymentRequest.getPaymentMethodRequest().getMethodType()) {
				case CREDIT_CARD	: 	createCreditCard(paymentRequest, processPayment);	break;
				case DEBIT_CARD		: 	createDebitCard(paymentRequest, processPayment);	break;
				case DEBIT_TICKET	:	createDebitTicket(paymentRequest, processPayment);	break;
			}

			return processPaymentRepository.save(processPayment);

		} catch (ProcessPaymentException e) {
			logger.error(e.getMessage());
		}
		return null;
	}

	/**
	 * Creates the debit ticket.
	 *
	 * @param paymentRequest
	 *            the payment request
	 * @param processPayment
	 *            the process payment
	 * @throws ProcessPaymentException
	 */
	private void createDebitTicket(PaymentRequest paymentRequest, ProcessPayment processPayment)
			throws ProcessPaymentException {
		try {
			Boleto boleto = new Boleto();
			boleto.setExpirationDate(BoletoMock.generateExpirationDate());
			boleto.setNumberBoleto(BoletoMock.generateBoletoNumber());
			processPayment.getPayment().setPaymentType(PaymentType.DEBIT_TICKET);
			processPayment.getPayment().setBoleto(boleto);
			processPayment.setAuthorizationStatus(AuthorizationStatus.PENDING);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ProcessPaymentException(e.getMessage());
		}
	}

	/**
	 * Creates the debit card.
	 *
	 * @param paymentRequest
	 *            the payment request
	 * @param processPayment
	 *            the process payment
	 * @throws ProcessPaymentException
	 */
	private void createDebitCard(PaymentRequest paymentRequest, ProcessPayment processPayment)
			throws ProcessPaymentException {
		try {
			DebitCard debitCard = new DebitCard();
			debitCard.setExpirationDate(DateUtils
					.toDateFromString(paymentRequest.getPaymentMethodRequest().getCardRequest().getExpirationDate()));
			debitCard.setName(paymentRequest.getPaymentMethodRequest().getCardRequest().getName());
			debitCard.setNumber(paymentRequest.getPaymentMethodRequest().getCardRequest().getNumber());
			processPayment.getPayment().setPaymentType(PaymentType.DEBIT_CARD);
			processPayment.getPayment().setCard(debitCard);
			processPayment.setAuthorizationStatus(paymentAuthorizationService.authorizationProcess(debitCard));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ProcessPaymentException(e.getMessage());
		}
	}

	/**
	 * Creates the credit card.
	 *
	 * @param paymentRequest
	 *            the payment request
	 * @param processPayment
	 *            the process payment
	 * @throws ProcessPaymentException
	 */
	private void createCreditCard(PaymentRequest paymentRequest, ProcessPayment processPayment)
			throws ProcessPaymentException {
		try {
			CreditCard creditCard = new CreditCard();
			creditCard.setCvv(paymentRequest.getPaymentMethodRequest().getCardRequest().getCvv());
			creditCard.setExpirationDate(DateUtils
					.toDateFromString(paymentRequest.getPaymentMethodRequest().getCardRequest().getExpirationDate()));
			creditCard.setName(paymentRequest.getPaymentMethodRequest().getCardRequest().getName());
			creditCard.setNumber(paymentRequest.getPaymentMethodRequest().getCardRequest().getNumber());
			processPayment.getPayment().setPaymentType(PaymentType.CREDIT_CARD);
			processPayment.getPayment().setCard(creditCard);
			processPayment.setAuthorizationStatus(paymentAuthorizationService.authorizationProcess(creditCard));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ProcessPaymentException(e.getMessage());
		}
	}

}
