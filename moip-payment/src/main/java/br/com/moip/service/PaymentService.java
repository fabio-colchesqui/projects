package br.com.moip.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.moip.entity.Payment;
import br.com.moip.repository.PaymentRepository;

/**
 * The Class PaymentService.
 */
@Component
public class PaymentService {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

	/** The payment repository. */
	@Autowired
	private PaymentRepository paymentRepository;

	/**
	 * Find payment.
	 *
	 * @param id
	 *            the id
	 * @return the optional
	 */
	public Optional<Payment> findPayment(Long id) {
		return paymentRepository.findById(id);
	}

}
