package br.com.moip.form;

/**
 * The Class PaymentOption.
 */
public class PaymentOption {

	/** The id. */
	private Long id;

	/** The opcao. */
	private String opcao;

	/**
	 * Instantiates a new payment option.
	 */
	public PaymentOption() {

	}

	/**
	 * Instantiates a new payment option.
	 *
	 * @param id
	 *            the id
	 * @param opcao
	 *            the opcao
	 */
	public PaymentOption(Long id, String opcao) {
		this.id = id;
		this.opcao = opcao;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id
	 *            the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the opcao.
	 *
	 * @return the opcao
	 */
	public String getOpcao() {
		return opcao;
	}

	/**
	 * Sets the opcao.
	 *
	 * @param opcao
	 *            the new opcao
	 */
	public void setOpcao(String opcao) {
		this.opcao = opcao;
	}

}
