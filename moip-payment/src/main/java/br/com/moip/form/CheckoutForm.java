package br.com.moip.form;

/**
 * The Class CheckoutForm.
 */
public class CheckoutForm {

	/** The url retorno. */
	private String urlRetorno;

	/** The id cliente. */
	private String idCliente;

	/** The nome buyer. */
	private String nomeBuyer;

	/** The email buyer. */
	private String emailBuyer;

	/** The cpf buyer. */
	private String cpfBuyer;

	/** The payment option id. */
	private String paymentOptionId;

	/** The amount. */
	private String amount;

	/** The card form. */
	private CardForm cardForm;

	/**
	 * Gets the url retorno.
	 *
	 * @return the url retorno
	 */
	public String getUrlRetorno() {
		return urlRetorno;
	}

	/**
	 * Sets the url retorno.
	 *
	 * @param urlRetorno
	 *            the new url retorno
	 */
	public void setUrlRetorno(String urlRetorno) {
		this.urlRetorno = urlRetorno;
	}

	/**
	 * Gets the id cliente.
	 *
	 * @return the id cliente
	 */
	public String getIdCliente() {
		return idCliente;
	}

	/**
	 * Sets the id cliente.
	 *
	 * @param idCliente
	 *            the new id cliente
	 */
	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	/**
	 * Gets the nome buyer.
	 *
	 * @return the nome buyer
	 */
	public String getNomeBuyer() {
		return nomeBuyer;
	}

	/**
	 * Sets the nome buyer.
	 *
	 * @param nomeBuyer
	 *            the new nome buyer
	 */
	public void setNomeBuyer(String nomeBuyer) {
		this.nomeBuyer = nomeBuyer;
	}

	/**
	 * Gets the email buyer.
	 *
	 * @return the email buyer
	 */
	public String getEmailBuyer() {
		return emailBuyer;
	}

	/**
	 * Sets the email buyer.
	 *
	 * @param emailBuyer
	 *            the new email buyer
	 */
	public void setEmailBuyer(String emailBuyer) {
		this.emailBuyer = emailBuyer;
	}

	/**
	 * Gets the cpf buyer.
	 *
	 * @return the cpf buyer
	 */
	public String getCpfBuyer() {
		return cpfBuyer;
	}

	/**
	 * Sets the cpf buyer.
	 *
	 * @param cpfBuyer
	 *            the new cpf buyer
	 */
	public void setCpfBuyer(String cpfBuyer) {
		this.cpfBuyer = cpfBuyer;
	}

	/**
	 * Gets the payment option id.
	 *
	 * @return the payment option id
	 */
	public String getPaymentOptionId() {
		return paymentOptionId;
	}

	/**
	 * Sets the payment option id.
	 *
	 * @param paymentOptionId
	 *            the new payment option id
	 */
	public void setPaymentOptionId(String paymentOptionId) {
		this.paymentOptionId = paymentOptionId;
	}

	/**
	 * Gets the amount.
	 *
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the amount.
	 *
	 * @param amount
	 *            the new amount
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * Gets the card form.
	 *
	 * @return the card form
	 */
	public CardForm getCardForm() {
		return cardForm;
	}

	/**
	 * Sets the card form.
	 *
	 * @param cardForm
	 *            the new card form
	 */
	public void setCardForm(CardForm cardForm) {
		this.cardForm = cardForm;
	}

}
