package br.com.moip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class MoipPaymentApplication.
 */
@SpringBootApplication
public class MoipPaymentApplication {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(MoipPaymentApplication.class, args);
	}
}
