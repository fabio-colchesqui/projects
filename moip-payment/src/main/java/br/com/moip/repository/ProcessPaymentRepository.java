package br.com.moip.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.moip.entity.ProcessPayment;

/**
 * The Interface ProcessPaymentRepository.
 */
@Repository
public interface ProcessPaymentRepository extends JpaRepository<ProcessPayment, Long> {

}
