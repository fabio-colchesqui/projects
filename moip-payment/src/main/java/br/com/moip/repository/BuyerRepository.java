package br.com.moip.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.moip.entity.Buyer;

/**
 * The Interface BuyerRepository.
 */
@Repository
public interface BuyerRepository extends JpaRepository<Buyer, String> {

	/**
	 * Find by cpf.
	 *
	 * @param cpf
	 *            the cpf
	 * @return the buyer
	 */
	public Buyer findByCpf(String cpf);

}
