package br.com.moip.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.moip.entity.Payment;

/**
 * The Interface PaymentRepository.
 */
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

}
