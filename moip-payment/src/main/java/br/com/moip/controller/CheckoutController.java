package br.com.moip.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.moip.entity.AuthorizationStatus;
import br.com.moip.entity.ProcessPayment;
import br.com.moip.form.CardForm;
import br.com.moip.form.CheckoutForm;
import br.com.moip.form.CheckoutRequest;
import br.com.moip.form.PaymentOption;
import br.com.moip.request.BuyerRequest;
import br.com.moip.request.CardRequest;
import br.com.moip.request.ClientRequest;
import br.com.moip.request.PaymentMethodRequest;
import br.com.moip.request.PaymentRequest;
import br.com.moip.service.ProcessPaymentService;

/**
 * The Class CheckoutController.
 */
@Controller
@RequestMapping("/CheckoutController")
public class CheckoutController {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(CheckoutController.class);
	
	/** The process payment service. */
	@Autowired
	private ProcessPaymentService processPaymentService;

	/**
	 * Checkout.
	 *
	 * @param checkoutRequest the checkout request
	 * @return the model and view
	 */
	@RequestMapping(value = "/checkout", method=RequestMethod.POST)
	public ModelAndView checkout(@ModelAttribute(value="checkoutRequest") CheckoutRequest checkoutRequest) {
		ModelAndView mv = new ModelAndView("/checkout");

		CheckoutForm checkoutForm = new CheckoutForm();
		checkoutForm.setCpfBuyer(checkoutRequest.getCpfBuyer());
		checkoutForm.setEmailBuyer(checkoutRequest.getEmailBuyer());
		checkoutForm.setIdCliente(checkoutRequest.getIdCliente());
		checkoutForm.setNomeBuyer(checkoutRequest.getNomeBuyer());
		checkoutForm.setUrlRetorno(checkoutRequest.getUrlRetorno());
		checkoutForm.setPaymentOptionId("1L");
		checkoutForm.setAmount(checkoutRequest.getAmount());
		checkoutForm.setCardForm(new CardForm());

		List<PaymentOption> listPaymentOption = Arrays.asList(new PaymentOption(1L, "Cartão de Crédito"),
															  new PaymentOption(2L, "Cartão de Débito"), 
															  new PaymentOption(3L, "Boleto"));

		mv.addObject("listPaymentOption", listPaymentOption);
		mv.addObject("checkoutForm", checkoutForm);

		return mv;
	}

	
	/**
	 * Process form.
	 *
	 * @param checkoutForm the checkout form
	 * @return the model and view
	 */
	@RequestMapping(value = "/processForm", method=RequestMethod.POST)
	public ModelAndView processForm(@ModelAttribute("checkoutForm") CheckoutForm checkoutForm) {
		PaymentRequest paymentRequest = new PaymentRequest();
		if(checkoutForm.getPaymentOptionId().equals("3")) {
			createBoletoPayment(checkoutForm, paymentRequest);
		} else {
			createCardPayment(checkoutForm, paymentRequest);
		}
		
		ProcessPayment processPayment = processPaymentService.processPayment(paymentRequest);
		ModelAndView mv = new ModelAndView("/processForm");
		
		if (processPayment.getAuthorizationStatus().equals(AuthorizationStatus.APPROVED)
				|| processPayment.getAuthorizationStatus().equals(AuthorizationStatus.PENDING)) {
			mv.addObject("status", "Pagamento processado com sucesso!");
		} else {
			mv.addObject("status", "Pagamento não autorizado!");
		}
		
		mv.addObject("urlRetorno", checkoutForm.getUrlRetorno());
		
		return mv;
	}


	/**
	 * Creates the card payment.
	 *
	 * @param checkoutForm the checkout form
	 * @param paymentRequest the payment request
	 */
	private void createCardPayment(CheckoutForm checkoutForm, PaymentRequest paymentRequest) {
		paymentRequest.buyer(new BuyerRequest()
				.buyerCpf(checkoutForm.getCpfBuyer())
				.buyerEmail(checkoutForm.getEmailBuyer())
				.buyerName(checkoutForm.getNomeBuyer()))
		.client(new ClientRequest()
				.client(new Long(checkoutForm.getIdCliente())))
		.paymentMethod(new PaymentMethodRequest()
				.creditCard(new CardRequest()
						.cvv(Long.getLong(checkoutForm.getCardForm().getCvv()))
						.expirationDate(checkoutForm.getCardForm().getExpirationDate())
						.name(checkoutForm.getCardForm().getName())
						.number(Long.getLong(checkoutForm.getCardForm().getNumber()))))
		.totalAmount(new BigDecimal(checkoutForm.getAmount()));
	}


	/**
	 * Creates the boleto payment.
	 *
	 * @param checkoutForm the checkout form
	 * @param paymentRequest the payment request
	 */
	private void createBoletoPayment(CheckoutForm checkoutForm, PaymentRequest paymentRequest) {
		paymentRequest.buyer(new BuyerRequest()
				.buyerCpf(checkoutForm.getCpfBuyer())
				.buyerEmail(checkoutForm.getEmailBuyer())
				.buyerName(checkoutForm.getNomeBuyer()))
		.client(new ClientRequest()
				.client(new Long(checkoutForm.getIdCliente())))
		.paymentMethod(new PaymentMethodRequest()
				.boleto())
		.totalAmount(new BigDecimal(checkoutForm.getAmount()));
	}

}
