package br.com.moip.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.moip.entity.AuthorizationStatus;
import br.com.moip.entity.ProcessPayment;
import br.com.moip.request.PaymentRequest;
import br.com.moip.response.AmountResponse;
import br.com.moip.response.AuthorizationStatusResponse;
import br.com.moip.response.BoletoResponse;
import br.com.moip.response.BuyerResponse;
import br.com.moip.response.PaymentResponse;
import br.com.moip.service.ProcessPaymentService;
import br.com.moip.utils.DateUtils;

/**
 * The Class ProcessPaymentController.
 */
@RestController
@RequestMapping("/ProcessPaymentController")
public class ProcessPaymentController {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(ProcessPaymentController.class);

	/** The process payment service. */
	@Autowired
	private ProcessPaymentService processPaymentService;

	/**
	 * Process payment.
	 *
	 * @param paymentRequest the payment request
	 * @return the payment response
	 */
	@PostMapping("/processPayment")
	public PaymentResponse processPayment(@RequestBody PaymentRequest paymentRequest) {
		ProcessPayment processPayment = processPaymentService.processPayment(paymentRequest);
		if (processPayment.getAuthorizationStatus().equals(AuthorizationStatus.DECLINED)) {
			return createResponse(paymentRequest);
		} else {
			return createResponse(processPayment);
		}
	}

	/**
	 * Creates the response.
	 *
	 * @param processPayment the process payment
	 * @return the payment response
	 */
	private PaymentResponse createResponse(ProcessPayment processPayment) {
		PaymentResponse response = new PaymentResponse();
		if (processPayment.getPayment().getBoleto() != null) {
			response.setBoletoResponse(new BoletoResponse());
			response.getBoletoResponse().setLineCode(processPayment.getPayment().getBoleto().getNumberBoleto());
			response.getBoletoResponse().setExpirationDate(
					DateUtils.toStringFromDate(processPayment.getPayment().getBoleto().getExpirationDate()));
		}

		response.setAmountReponse(new AmountResponse());
		response.getAmountReponse().setTotalAmount(processPayment.getPayment().getAmount().getTotalAmount());

		if (processPayment.getAuthorizationStatus().equals(AuthorizationStatus.APPROVED)) {
			response.setAuthorizationStatusResponse(AuthorizationStatusResponse.APPROVED);
		} else {
			response.setAuthorizationStatusResponse(AuthorizationStatusResponse.PENDING);
		}

		response.setIdClient(processPayment.getClient().getId());

		response.setBuyerResponse(new BuyerResponse());
		response.getBuyerResponse().setCpf(processPayment.getBuyer().getCpf());
		response.getBuyerResponse().setName(processPayment.getBuyer().getName());

		return response;
	}

	/**
	 * Creates the response.
	 *
	 * @param paymentRequest the payment request
	 * @return the payment response
	 */
	private PaymentResponse createResponse(PaymentRequest paymentRequest) {
		PaymentResponse response = new PaymentResponse();
		response.setAmountReponse(new AmountResponse());
		response.getAmountReponse().setTotalAmount(paymentRequest.getTotalAmount());
		response.setAuthorizationStatusResponse(AuthorizationStatusResponse.DECLINED);

		response.setIdClient(paymentRequest.getClientRequest().getIdClient());

		response.setBuyerResponse(new BuyerResponse());
		response.getBuyerResponse().setCpf(paymentRequest.getBuyerRequest().getCpf());
		response.getBuyerResponse().setName(paymentRequest.getBuyerRequest().getName());

		return response;
	}

}
