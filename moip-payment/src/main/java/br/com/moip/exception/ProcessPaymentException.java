package br.com.moip.exception;

/**
 * The Class ProcessPaymentException.
 */
public class ProcessPaymentException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new payment not found exception.
	 *
	 * @param message
	 *            the message
	 */
	public ProcessPaymentException(String message) {
		super(message);
	}

}
