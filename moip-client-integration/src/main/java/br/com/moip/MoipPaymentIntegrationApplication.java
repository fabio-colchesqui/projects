package br.com.moip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class MoipPaymentIntegrationApplication.
 */
@SpringBootApplication
public class MoipPaymentIntegrationApplication {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(MoipPaymentIntegrationApplication.class, args);
	}
}
