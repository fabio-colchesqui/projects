package br.com.moip.controller;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.moip.api.connector.MoipConnector;
import br.com.moip.api.exception.MoipApiException;
import br.com.moip.request.BuyerRequest;
import br.com.moip.request.CardRequest;
import br.com.moip.request.ClientRequest;
import br.com.moip.request.PaymentMethodRequest;
import br.com.moip.request.PaymentRequest;
import br.com.moip.response.PaymentResponse;

/**
 * The Class PaymentController.
 */
@RestController
@RequestMapping("/PaymentController")
public class PaymentController {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

	/** The moip connector. */
	private MoipConnector moipConnector;
	
	/**
	 * Pagamento boleto.
	 *
	 * @return the payment response
	 * @throws MoipApiException the moip api exception
	 */
	@RequestMapping(value = "/pagamentoBoleto", method = RequestMethod.GET)
	public PaymentResponse pagamentoBoleto() throws MoipApiException {
		if (moipConnector == null)
			moipConnector = new MoipConnector();

		PaymentRequest request = new PaymentRequest();
		request.buyer(new BuyerRequest()
				.buyerCpf("200.201.202-20")
				.buyerEmail("jose.almeida@teste.com.br")
				.buyerName("JOSE ALMEIDA"))
		.client(new ClientRequest()
				.client(1001L))
		.paymentMethod(new PaymentMethodRequest()
				.boleto())
		.totalAmount(new BigDecimal("1000.00"));

		return moipConnector.submitPayment(request);
	}

	/**
	 * Pagamento cartao credito.
	 *
	 * @return the payment response
	 * @throws MoipApiException the moip api exception
	 */
	@RequestMapping(value = "/pagamentoCartaoCredito", method = RequestMethod.GET)
	public PaymentResponse pagamentoCartaoCredito() throws MoipApiException {
		if (moipConnector == null)
			moipConnector = new MoipConnector();

		PaymentRequest request = new PaymentRequest();
		request.buyer(new BuyerRequest()
				.buyerCpf("200.201.202-20")
				.buyerEmail("jose.almeida@teste.com.br")
				.buyerName("JOSE ALMEIDA"))
		.client(new ClientRequest()
				.client(1002L))
		.paymentMethod(new PaymentMethodRequest()
				.creditCard(new CardRequest()
						.cvv(777L)
						.expirationDate("23/01/2020")
						.name("JOSE ALMEIDA")
						.number(100234679009209L)))
		.totalAmount(new BigDecimal("1500.00"));

		return moipConnector.submitPayment(request);
	}
	
	/**
	 * Pagamento cartao debito.
	 *
	 * @return the payment response
	 * @throws MoipApiException the moip api exception
	 */
	@RequestMapping(value = "/pagamentoCartaoDebito", method = RequestMethod.GET)
	public PaymentResponse pagamentoCartaoDebito() throws MoipApiException {
		if (moipConnector == null)
			moipConnector = new MoipConnector();

		PaymentRequest request = new PaymentRequest();
		request.buyer(new BuyerRequest()
				.buyerCpf("200.201.202-20")
				.buyerEmail("jose.almeida@teste.com.br")
				.buyerName("JOSE ALMEIDA"))
		.client(new ClientRequest()
				.client(1003L))
		.paymentMethod(new PaymentMethodRequest()
				.debitCard(new CardRequest()
						.cvv(777L)
						.expirationDate("23/01/2020")
						.name("JOSE ALMEIDA")
						.number(200254677012901L)))
		.totalAmount(new BigDecimal("2000.00"));

		return moipConnector.submitPayment(request);
	}

}
