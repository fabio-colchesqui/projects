var app = angular.module('moipIntegration', [
  'ngRoute'
]);

app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    .when("/", {templateUrl: "partials/home.html", controller: "MainController"}) 
    .when("/java", {templateUrl: "partials/pgjava.html", controller: "JavaController"})
    .when("/html", {templateUrl: "partials/pghtml.html", controller: "HtmlController"});
}]);

app.controller('MainController', function (/* $scope, $location, $http */) {
});

app.controller('HtmlController', function (/* $scope, $location, $http */) {
});

app.controller('JavaController', function ( $scope, $location, $http ) {
	
	$scope.pagamentoBoleto = function() {
		$http.get("/PaymentController/pagamentoBoleto")
				.success(function(data) {
					$scope.retornoPost = data;
				}).error(function(error) {
					console.log(error);
					console.log("error!");
				});
	};

	$scope.pagamentoCartaoCredito = function() {
		$http.get("/PaymentController/pagamentoCartaoCredito")
				.success(function(data) {
					$scope.retornoPost = data;
				}).error(function(error) {
					console.log(error);
					console.log("error!");
				});
	};

	$scope.pagamentoCartaoDebito = function() {
		$http.get("/PaymentController/pagamentoCartaoDebito")
				.success(function(data) {
					$scope.retornoPost = data;
				}).error(function(error) {
					console.log(error);
					console.log("error!");
				});
	};
	
});

