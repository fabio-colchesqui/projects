package br.com.moip.api.connector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

import br.com.moip.api.exception.MoipApiException;
import br.com.moip.request.PaymentRequest;
import br.com.moip.response.PaymentResponse;

/**
 * The Class MoipConnector.
 */
public class MoipConnector {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(MoipConnector.class);

	/**
	 * Submit payment.
	 *
	 * @param paymentRequest
	 *            the payment request
	 * @return the payment response
	 * @throws MoipApiException
	 */
	public PaymentResponse submitPayment(PaymentRequest paymentRequest) throws MoipApiException {
		try {
			Client client = Client.create();
			WebResource webResource = client.resource("http://localhost:8888/ProcessPaymentController/processPayment");
			Builder builder = webResource.type("application/json");
			builder.accept("application/json");
			return builder.post(PaymentResponse.class, paymentRequest);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new MoipApiException(e.getMessage());
		}
	}

}
