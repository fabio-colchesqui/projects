package br.com.moip.api.exception;

/**
 * The Class MoipApiException.
 */
public class MoipApiException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new moip api exception.
	 *
	 * @param message
	 *            the message
	 */
	public MoipApiException(String message) {
		super(message);
	}

}
